import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SectorService {
  private readonly BASE_URL = 'http://localhost:8081/api/v1/sectors/';
  private readonly GET = 'getAll';

  constructor(private http: HttpClient) { }

  getSectors(): Observable<any> {
    return this.http.get(this.BASE_URL + this.GET);
  }

  handleSectors() {

  }
}
