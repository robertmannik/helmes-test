import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Form} from "../model/Form";

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private readonly BASE_URL = 'http://localhost:8081/api/v1/form/';
  private readonly GET = 'get/';
  private readonly ADD = 'addOrUpdate';

  constructor(private http: HttpClient) {
  }

  getForm(id: string): Observable<any> {
    return this.http.get(this.BASE_URL + this.GET + id);
  }

  addForm(form: Form): Observable<any> {
    return this.http.post(this.BASE_URL + this.ADD, form)
  }
}
