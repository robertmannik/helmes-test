export class Sector {
  id: string;
  name: string;
  subSectors: Array<Sector>;
}
