import {Form} from "./Form";

export class GetForm {
  status: string;
  payload: Array<Form>
}
