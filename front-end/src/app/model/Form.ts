export class Form {
  id: string;
  name: string;
  agreedToTerms: boolean;
  sectorIds: Array<string>;
}
