import {Component, OnInit} from '@angular/core';
import {FormService} from "./service/form.service";
import {SectorService} from "./service/sector.service";
import {Sector} from "./model/Sector";
import {Form} from "./model/Form";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  public showSuccessMsg: boolean;
  public model: Form;
  public sectors: Array<Sector>;
  private readonly SECTOR_NAME_PREFIX = "&nbsp;&nbsp;&nbsp;&nbsp;";

  title = 'front-end';

  constructor(private formService: FormService, private sectorService: SectorService,) {
    this.formService = formService;
    this.sectorService = sectorService;
    this.showSuccessMsg =false;
  }

  ngOnInit() {
    if (localStorage.getItem("form")) {
      this.model = JSON.parse(localStorage.getItem("form"));
    } else {
      this.model = new Form();
    }
    this.sectorService.getSectors().subscribe(data => {
      this.sectors = this.handleSectors(null, data.payload);
    });
  }

  onCheckboxChange(e) {

  }

  onSubmit() {
    this.formService.addForm(this.model).subscribe(response => {
      this.model = response.payload;
      localStorage.setItem("form", JSON.stringify(this.model));
      this.showSuccessMsg = true;
    });
  }

  private handleSectors(prefix: string, sectors: Array<Sector>) {
    let newSectors = new Array<Sector>();
    sectors.forEach(sector => {
      if (!!prefix) {
        sector.name = prefix + sector.name;
      }
      newSectors.push(sector);
      if (sector.subSectors.length > 0) {
        let editedSubSectors = this.handleSectors(!!prefix ? prefix + this.SECTOR_NAME_PREFIX : this.SECTOR_NAME_PREFIX, sector.subSectors);
        editedSubSectors.forEach(editedSubSector => {
          newSectors.push(editedSubSector);
        })
      }
    });
    return newSectors;
  }

  containsForm() {
    return !!localStorage.getItem("form");
  }

  clearFormInLocalStorage() {
    localStorage.removeItem("form");
  }
}
