package com.formhandler.controller.v1.api;

import com.formhandler.controller.v1.request.AddOrUpdateFormRequest;
import com.formhandler.dto.form.FormDto;
import com.formhandler.dto.response.Response;
import com.formhandler.service.FormService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FormControllerTest {
	@Mock
	private FormService formService;

	@InjectMocks
	private FormController formController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addShouldCallService() {
		String name = "test";
		List<UUID> uuids = List.of(UUID.randomUUID());

		AddOrUpdateFormRequest addOrUpdateFormRequest = new AddOrUpdateFormRequest()
				.setAgreedToTerms(true)
				.setName(name)
				.setSectorIds(uuids);

		FormDto formDto = new FormDto()
				.setAgreedToTerms(true)
				.setName(name)
				.setSectorIds(uuids);

		when(formService.addOrUpdateForm(addOrUpdateFormRequest.getId(), addOrUpdateFormRequest.getName(), addOrUpdateFormRequest.isAgreedToTerms(), addOrUpdateFormRequest
				.getSectorIds())).thenReturn(formDto);
		Response result = formController.addOrUpdate(addOrUpdateFormRequest);
		FormDto payload = (FormDto) result.getPayload();
		verify(formService).addOrUpdateForm(addOrUpdateFormRequest.getId(), addOrUpdateFormRequest.getName(), addOrUpdateFormRequest.isAgreedToTerms(), addOrUpdateFormRequest
				.getSectorIds());
		assertEquals(Response.Status.OK, result.getStatus());
		assertEquals(name, payload.getName());
		assertTrue(payload.isAgreedToTerms());
		assertEquals(uuids.get(0), payload.getSectorIds().get(0));
	}

	@Test
	public void addShouldReturnBadRequest() {
		AddOrUpdateFormRequest addOrUpdateFormRequest = new AddOrUpdateFormRequest();

		Response result = formController.addOrUpdate(addOrUpdateFormRequest);
		FormDto payload = (FormDto) result.getPayload();

		assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
		assertNull(payload);
	}

	@Test
	public void getShouldCallService() {
		UUID uuid = UUID.randomUUID();
		String name = "test";
		List<UUID> uuids = List.of(UUID.randomUUID());

		FormDto formDto = new FormDto()
				.setId(uuid)
				.setAgreedToTerms(true)
				.setName(name)
				.setSectorIds(uuids);

		when(formService.getById(uuid)).thenReturn(formDto);
		Response result = formController.getById(uuid);
		FormDto payload = (FormDto) result.getPayload();
		verify(formService).getById(uuid);
		assertEquals(Response.Status.OK, result.getStatus());
		assertEquals(uuid, payload.getId());
		assertEquals(name, payload.getName());
		assertTrue(payload.isAgreedToTerms());
		assertEquals(uuids.get(0), payload.getSectorIds().get(0));
	}

	@Test
	public void getShouldReturnBadRequest() {;
		when(formService.getById(null)).thenReturn(null);
		Response result = formController.getById(null);
		FormDto payload = (FormDto) result.getPayload();

		assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
		assertNull(payload);
	}
}