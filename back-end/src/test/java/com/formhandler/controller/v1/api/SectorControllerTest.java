package com.formhandler.controller.v1.api;

import com.formhandler.controller.v1.request.AddSectorRequest;
import com.formhandler.dto.response.Response;
import com.formhandler.dto.sector.SectorDto;
import com.formhandler.service.SectorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SectorControllerTest {
	@Mock
	private SectorService sectorService;

	@InjectMocks
	private SectorController sectorController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getSectorsShouldCalLServiceAndReturnSectors() {
		UUID parentId = UUID.randomUUID();
		UUID childId = UUID.randomUUID();
		String parentName = "test";
		String childName = "child";

		SectorDto sectorDto = new SectorDto()
				.setId(parentId)
				.setName(parentName);

		SectorDto childDto = new SectorDto()
				.setId(childId)
				.setParentId(parentId)
				.setName(childName);

		sectorDto.setSubSectors(List.of(childDto));

		when(sectorService.getAllSectors()).thenReturn(List.of(sectorDto));
		Response result = sectorController.getSectors();
		List<SectorDto> payload = (List<SectorDto>) result.getPayload();
		verify(sectorService).getAllSectors();
		assertEquals(Response.Status.OK, result.getStatus());
		assertEquals(1, payload.size());
		assertEquals(parentId, payload.get(0).getId());
		assertEquals(parentName, payload.get(0).getName());
		assertEquals(childName, payload.get(0).getSubSectors().get(0).getName());
		assertEquals(childId, payload.get(0).getSubSectors().get(0).getId());
		assertEquals(parentId, payload.get(0).getSubSectors().get(0).getParentId());
	}

	@Test
	public void getSectorsShouldReturnNotFoundRequest() {
		when(sectorService.getAllSectors()).thenReturn(Collections.emptyList());
		Response result = sectorController.getSectors();
		List<SectorDto> payload = (List<SectorDto>) result.getPayload();
		assertEquals(Response.Status.NOT_FOUND, result.getStatus());
		assertNull(payload);
	}

	@Test
	public void addShouldCallServiceAndReturnSector() {
		UUID id = UUID.randomUUID();
		String name = "test";

		SectorDto sectorDto = new SectorDto()
				.setId(id)
				.setName(name);

		AddSectorRequest addSectorRequest = new AddSectorRequest().setName(name);

		when(sectorService.addSector(name, null)).thenReturn(sectorDto);
		Response result = sectorController.add(addSectorRequest);
		SectorDto payload = (SectorDto) result.getPayload();
		verify(sectorService).addSector(name, null);
		assertEquals(Response.Status.OK, result.getStatus());
		assertEquals(id, payload.getId());
		assertEquals(name, payload.getName());
	}

	@Test
	public void addShouldReturnBadRequest() {
		AddSectorRequest addSectorRequest = new AddSectorRequest();

		Response result = sectorController.add(addSectorRequest);
		SectorDto payload = (SectorDto) result.getPayload();
		assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
		assertNull(payload);
	}

	@Test
	public void addShouldReturnBadRequestForInvalidParentId() {
		UUID parentId = UUID.randomUUID();
		String name = "test";

		AddSectorRequest addSectorRequest = new AddSectorRequest()
				.setName(name)
				.setParentId(parentId);

		when(sectorService.addSector(name, parentId)).thenReturn(null);
		Response result = sectorController.add(addSectorRequest);
		SectorDto payload = (SectorDto) result.getPayload();
		assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
		assertNull(payload);
	}
}