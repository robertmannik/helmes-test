package com.formhandler.repository;

import com.formhandler.model.Sector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SectorDaoTest {
	@Mock
	private EntityManager entityManager;

	@InjectMocks
	private SectorDao sectorDao = new SectorDao(entityManager);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addFormShouldAddAndReturnSector() {
		UUID sectorId = UUID.randomUUID();
		String name = "test";
		Sector sector = new Sector().setId(sectorId).setName(name);

		doNothing().when(entityManager).persist(sector);
		Sector result = sectorDao.addSector(sector);
		verify(entityManager).persist(sector);
		assertEquals(sectorId, result.getId());
		assertEquals(name, result.getName());
	}

	@Test
	public void getAllShouldReturnSectorList() {
		UUID sectorId = UUID.randomUUID();
		String name = "test";
		Sector sector = new Sector().setId(sectorId).setName(name);

		TypedQuery query = mock(TypedQuery.class);
		when(entityManager.createQuery(anyString(), any())).thenReturn(query);

		when(query.getResultList()).thenReturn(List.of(sector));
		List<Sector> result = sectorDao.getAllSectors();
		assertEquals(name, result.get(0).getName());
		assertEquals(sectorId, result.get(0).getId());
	}

	@Test
	public void getSectorByIdShouldReturnSector() {
		UUID sectorId = UUID.randomUUID();
		String name = "test";
		Sector sector = new Sector().setId(sectorId).setName(name);

		TypedQuery query = mock(TypedQuery.class);
		when(entityManager.createQuery(anyString(), any())).thenReturn(query);
		when(query.setParameter("id", sector)).thenReturn(query);
		when(query.getSingleResult()).thenReturn(sector);
		Sector result = sectorDao.getSectorById(sectorId);
		assertEquals(name, result.getName());
		assertEquals(sectorId, result.getId());
	}
}