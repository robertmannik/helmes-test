package com.formhandler.repository;

import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import com.formhandler.model.Sector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FormSectorDaoTest {
	@Mock
	private EntityManager entityManager;

	@InjectMocks
	private FormSectorDao formSectorDao = new FormSectorDao(entityManager);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addFormShouldAddIdAndReturnFormSector() {
		UUID sectorId = UUID.randomUUID();
		UUID formId = UUID.randomUUID();
		Form form = new Form().setId(formId);
		Sector sector = new Sector().setId(sectorId);
		UUID id = UUID.randomUUID();
		FormSector formsSector = new FormSector().setId(id).setForm(form).setSector(sector);

		doNothing().when(entityManager).persist(formsSector);
		FormSector result = formSectorDao.addFormSector(formsSector);
		verify(entityManager).persist(formsSector);
		assertEquals(id, result.getId());
		assertEquals(sectorId, result.getSector().getId());
		assertEquals(formId, result.getForm().getId());
	}

	@Test
	public void getFormSectorsByFormIdShouldReturnFormSector() {
		UUID sectorId = UUID.randomUUID();
		UUID formId = UUID.randomUUID();
		Form form = new Form().setId(formId);
		Sector sector = new Sector().setId(sectorId);
		UUID id = UUID.randomUUID();
		FormSector formSector = new FormSector().setId(id).setForm(form).setSector(sector);

		TypedQuery query = mock(TypedQuery.class);
		when(entityManager.createQuery(anyString(), any())).thenReturn(query);
		when(query.setParameter("id", formId)).thenReturn(query);
		when(query.getResultList()).thenReturn(List.of(formSector));
		List<FormSector> result = formSectorDao.getFormSectorsByFormId(formId);
		assertEquals(id, result.get(0).getId());
		assertEquals(sectorId, result.get(0).getSector().getId());
		assertEquals(formId, result.get(0).getForm().getId());
	}

	@Test
	public void removeShouldRemoveFormSector() {
		UUID sectorId = UUID.randomUUID();
		UUID formId = UUID.randomUUID();
		Form form = new Form().setId(formId);
		Sector sector = new Sector().setId(sectorId);
		UUID id = UUID.randomUUID();
		FormSector formSector = new FormSector().setId(id).setForm(form).setSector(sector);

		doNothing().when(entityManager).remove(formSector);
		formSectorDao.remove(formSector);
		verify(entityManager).remove(formSector);
	}
}