package com.formhandler.repository;

import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FormDaoTest {
	@Mock
	private EntityManager entityManager;

	@InjectMocks
	private FormDao formDao = new FormDao(entityManager);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addFormShouldAddIdAndReturnForm() {
		String name = "test";
		Form form = new Form()
				.setName(name).setAgreedToTerms(true).setFormSectors(List.of(new FormSector()));
		doNothing().when(entityManager).persist(form);
		Form result = formDao.addOrUpdateForm(form);
		verify(entityManager).persist(form);
		assertEquals(name, result.getName());
	}

	@Test
	public void addFormShouldUpdateReturnForm() {
		String name = "test";
		UUID id = UUID.randomUUID();
		Form form = new Form()
				.setId(id).setName(name).setAgreedToTerms(true).setFormSectors(List.of(new FormSector()));
		when(entityManager.merge(form)).thenReturn(form);
		Form result = formDao.addOrUpdateForm(form);
		verify(entityManager).merge(form);
		assertEquals(name, result.getName());
		assertEquals(id, result.getId());
	}

	@Test
	public void getByIdShouldReturnForm() {
		UUID uuid = UUID.randomUUID();
		String name = "test";
		Form form = new Form()
				.setId(uuid)
				.setName(name)
				.setAgreedToTerms(true)
				.setFormSectors(List.of(new FormSector()));
		TypedQuery query = mock(TypedQuery.class);
		when(entityManager.createQuery(anyString(), any())).thenReturn(query);
		when(query.setParameter("id", uuid)).thenReturn(query);
		when(query.getSingleResult()).thenReturn(form);
		Form result = formDao.getById(uuid);
		assertEquals(name, result.getName());
		assertEquals(uuid, result.getId());
		assertTrue(result.isAgreedToTerms());
		assertEquals(1, result.getFormSectors().size());
	}
}