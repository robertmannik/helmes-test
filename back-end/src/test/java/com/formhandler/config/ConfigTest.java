package com.formhandler.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.FilterConfig;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ConfigTest {
	@Mock
	private Environment env;

	@InjectMocks
	private Config config = new Config(env);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldGetProperties() {
		String works = "works";
		String getWorks = "getWorks";
		when(env.getProperty(getWorks)).thenReturn(works);
		String result = config.getConfigValue(getWorks);
		assertEquals(works, result);
	}

	@Test
	public void shouldGetCorsFilter() {
		config.corsFilter();
	}
}