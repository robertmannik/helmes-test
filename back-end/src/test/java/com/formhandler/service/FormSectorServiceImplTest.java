package com.formhandler.service;

import com.formhandler.dto.formsector.FormSectorDto;
import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import com.formhandler.model.Sector;
import com.formhandler.repository.FormSectorDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FormSectorServiceImplTest {
	@Mock
	private FormSectorDao formSectorDao;

	@InjectMocks
	private FormSectorService formSectorService = new FormSectorServiceImpl(formSectorDao);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addFormSectorShouldReturnDto() {
		UUID formModelId = UUID.randomUUID();
		Form formModel = new Form()
				.setId(formModelId);

		UUID sectorModelId = UUID.randomUUID();
		Sector sectorModel = new Sector()
				.setId(sectorModelId);

		UUID formSectorId = UUID.randomUUID();
		FormSector formSector = new FormSector()
				.setId(formSectorId)
				.setSector(sectorModel)
				.setForm(formModel);

		when(formSectorDao.addFormSector(any())).thenReturn(formSector);

		FormSectorDto result = formSectorService.addFormSector(formModel, sectorModel);

		assertEquals(formModel.getId(), result.getFormId());
		assertEquals(sectorModel.getId(), result.getSectorId());
	}

	@Test
	public void getFormSectorsByFormIdShouldCallDao() {
		UUID uuid = UUID.randomUUID();
		List<FormSector> formSectors = new ArrayList<>();

		when(formSectorDao.getFormSectorsByFormId(uuid)).thenReturn(formSectors);

		List<FormSector> result = formSectorService.getFormSectorsByFormId(uuid);
		verify(formSectorDao).getFormSectorsByFormId(uuid);
		assertEquals(formSectors.size(), result.size());
	}

	@Test
	public void addOrUpdateFormSectorsShouldAddFormSectors() {
		Sector sector = new Sector().setId(UUID.randomUUID());
		List<Sector> sectors = List.of(sector);
		List<UUID> sectorIds = List.of(sector.getId());
		Form form = new Form().setId(UUID.randomUUID()).setName("test");

		when(formSectorDao.getFormSectorsByFormId(form.getId())).thenReturn(Collections.emptyList());

		formSectorService.addOrUpdateFormSectors(form, sectors, sectorIds);
		verify(formSectorDao, times(1)).addFormSector(any());
	}

	@Test
	public void addOrUpdateFormSectorsShouldAddNewAndRemoveOutdatedFormSectors() {
		Sector sectorToBeAdded = new Sector().setId(UUID.randomUUID());
		List<Sector> sectorsToBeAdded = List.of(sectorToBeAdded);
		List<UUID> sectorToBeAddedIds = List.of(sectorToBeAdded.getId());

		Sector sectorToBeRemoved = new Sector().setId(UUID.randomUUID());
		Form form = new Form().setId(UUID.randomUUID()).setName("test");

		FormSector formSectorToBeRemoved = new FormSector().setSector(sectorToBeRemoved).setForm(form);

		when(formSectorDao.getFormSectorsByFormId(form.getId())).thenReturn(List.of(formSectorToBeRemoved));

		formSectorService.addOrUpdateFormSectors(form, sectorsToBeAdded, sectorToBeAddedIds);
		verify(formSectorDao, times(1)).addFormSector(any());
		verify(formSectorDao, times(1)).remove(any());
	}
}