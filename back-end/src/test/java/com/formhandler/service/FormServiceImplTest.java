package com.formhandler.service;

import com.formhandler.config.ErrorConfig;
import com.formhandler.dto.form.FormDto;
import com.formhandler.dto.formsector.FormSectorDto;
import com.formhandler.exception.FormHandlerException;
import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import com.formhandler.model.Sector;
import com.formhandler.repository.FormDao;
import com.formhandler.repository.FormSectorDao;
import com.formhandler.repository.SectorDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
public class FormServiceImplTest {
	@Mock
	private FormDao formDao;

	@Mock
	private SectorDao sectorDao;

	@Mock
	private FormSectorDao formSectorDao;

	@InjectMocks
	FormHandlerException formHandlerException;

	@Mock
	private ErrorConfig errorConfig;

	@Mock
	private SectorService sectorService = new SectorServiceImpl(sectorDao);

	@Mock
	private FormSectorService formSectorService = new FormSectorServiceImpl(formSectorDao);

	@InjectMocks
	private FormService formService = new FormServiceImpl(formDao, sectorService, formSectorService);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(formHandlerException, "errorConfig", errorConfig);
	}

	@Test
	public void addFormShouldReturnDto() {
		String name = "test";
		UUID sectorId = UUID.randomUUID();
		UUID formId = UUID.randomUUID();
		List<UUID> sectorIds = List.of(sectorId);
		Sector sector = new Sector().setId(sectorId);

		FormSectorDto formSector = new FormSectorDto()
				.setId(UUID.randomUUID())
				.setSectorId(sectorId);

		Form form = new Form().setName(name).setAgreedToTerms(true);

		when(sectorService.getSectorById(sectorId)).thenReturn(sector);

		when(formSectorService.addFormSector(any(Form.class), eq(sector))).thenReturn(formSector);
		when(formDao.addOrUpdateForm(any())).thenReturn(form);
		FormDto result = formService.addOrUpdateForm(formId, name, true, sectorIds);
		assertEquals(formId, result.getId());
		assertEquals(name, result.getName());
		assertTrue(result.isAgreedToTerms());
		assertEquals(sectorId, result.getSectorIds().get(0));
	}

	@Test
	public void getByIdShouldReturnForm() {
		UUID formId = UUID.randomUUID();
		UUID sectorId = UUID.randomUUID();
		Form form =  new Form().setId(formId);
		Sector sector = new Sector().setId(sectorId);
		FormSector formSector = new FormSector().setForm(form).setSector(sector);

		when(formSectorService.getFormSectorsByFormId(formId)).thenReturn(List.of(formSector));

		when(formDao.getById(formId)).thenReturn(form);
		FormDto result = formService.getById(formId);
		assertEquals(formId, result.getId());
		assertEquals(sectorId, result.getSectorIds().get(0));
	}

	@Test(expected = FormHandlerException.EntityNotFoundException.class)
	public void getByIdShouldThrowError() {
		when(formDao.getById(any())).thenReturn(null);
		formService.getById(UUID.randomUUID());
	}
}