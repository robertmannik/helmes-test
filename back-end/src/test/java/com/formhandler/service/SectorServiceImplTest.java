package com.formhandler.service;

import com.formhandler.config.ErrorConfig;
import com.formhandler.dto.sector.SectorDto;
import com.formhandler.exception.FormHandlerException;
import com.formhandler.model.Sector;
import com.formhandler.repository.SectorDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SectorServiceImplTest {
	@Mock
	private SectorDao sectorDao;

	@Mock
	private ErrorConfig errorConfig;

	@InjectMocks
	private SectorService sectorService = new SectorServiceImpl(sectorDao);

	@InjectMocks
	FormHandlerException formHandlerException;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(formHandlerException, "errorConfig", errorConfig);
	}

	@Test
	public void getAllSectorsShouldCallDao() {
		when(sectorDao.getAllSectors()).thenReturn(new ArrayList<>());
		List<SectorDto> sectorList = sectorService.getAllSectors();

		verify(sectorDao).getAllSectors();
		assertEquals(0, sectorList.size());
	}

	@Test
	public void addSectorShouldReturnSectorDto() {
		String name = "test";
		UUID id = UUID.randomUUID();
		Sector sector = new Sector().setId(id).setName(name);
		when(sectorDao.addSector(any())).thenReturn(sector);
		SectorDto sectorDto = sectorService.addSector(name, null);

		assertEquals(id, sectorDto.getId());
		assertEquals(name, sectorDto.getName());
	}

	@Test
	public void addSectorShouldReturnSectorDtoWithParentId() {
		String name = "test";
		String parentName = "parent";
		UUID id = UUID.randomUUID();
		UUID parentId = UUID.randomUUID();
		Sector sector = new Sector().setId(id).setName(name);
		Sector parentSector = new Sector().setId(parentId).setName(parentName);
		sector.setParent(parentSector);
		when(sectorDao.addSector(any())).thenReturn(sector);
		when(sectorDao.getSectorById(parentId)).thenReturn(parentSector);
		SectorDto sectorDto = sectorService.addSector(name, parentId);

		assertEquals(id, sectorDto.getId());
		assertEquals(name, sectorDto.getName());
		assertEquals(parentId, sectorDto.getParentId());
	}

	@Test(expected = FormHandlerException.EntityNotFoundException.class)
	public void addSectorShouldThrowEntityNotFoundException() {
		String name = "test";
		UUID id = UUID.randomUUID();
		UUID parentId = UUID.randomUUID();
		Sector sector = new Sector().setId(id).setName(name);
		when(sectorDao.addSector(any())).thenReturn(sector);
		when(sectorDao.getSectorById(parentId)).thenReturn(null);
		when(errorConfig.getConfigValue("sector.not.found")).thenReturn("Requested sector with id {0} does not exist.");
		sectorService.addSector(name, parentId);
	}
}