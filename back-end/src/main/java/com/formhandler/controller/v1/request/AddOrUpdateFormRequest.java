package com.formhandler.controller.v1.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateFormRequest {
	private UUID id;

	@Size(min = 2, max = 255, message = "Name must be between 2 and 255 characters long!")
	private String name;

	@NotNull(message = "Terms must be agreed to!")
	private boolean agreedToTerms;

	private List<UUID> sectorIds;
}
