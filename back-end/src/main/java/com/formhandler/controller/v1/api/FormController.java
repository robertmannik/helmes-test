package com.formhandler.controller.v1.api;

import com.formhandler.controller.v1.request.AddOrUpdateFormRequest;
import com.formhandler.dto.form.FormDto;
import com.formhandler.dto.response.Response;
import com.formhandler.service.FormService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/form")
@AllArgsConstructor
public class FormController {
	private FormService formService;

	@PostMapping("/addOrUpdate")
	public Response addOrUpdate(@RequestBody @Valid AddOrUpdateFormRequest addOrUpdateFormRequest) {
		Optional<FormDto> formDto = Optional.ofNullable(formService
				.addOrUpdateForm(addOrUpdateFormRequest.getId(), addOrUpdateFormRequest.getName(), addOrUpdateFormRequest.isAgreedToTerms(), addOrUpdateFormRequest
						.getSectorIds()));

		if (formDto.isPresent()) {
			return Response.ok().setPayload(formDto.get());
		}
		return Response.badRequest()
				.setErrors(String.format("Unable to save form with name; %s", addOrUpdateFormRequest.getName()));
	}

	@GetMapping("/get/{id}")
	public Response getById(@PathVariable @Valid UUID id) {
		Optional<FormDto> formDto = Optional.ofNullable(formService.getById(id));
		if (formDto.isPresent()) {
			return Response.ok().setPayload(formDto.get());
		}
		return Response.badRequest()
				.setErrors(String.format("Unable to save form with id: %s", id));
	}
}
