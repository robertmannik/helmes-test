package com.formhandler.controller.v1.api;

import com.formhandler.controller.v1.request.AddSectorRequest;
import com.formhandler.dto.response.Response;
import com.formhandler.dto.sector.SectorDto;
import com.formhandler.service.SectorService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/sectors")
@AllArgsConstructor
public class SectorController {
	private SectorService sectorService;

	@GetMapping("/getAll")
	public Response getSectors() {
		List<SectorDto> sectors = sectorService.getAllSectors();
		if (!sectors.isEmpty()) {
			return Response.ok().setPayload(sectors);
		}
		return Response.notFound()
				.setErrors("No sectors found!");
	}

	@PostMapping("/add")
	public Response add(@RequestBody @Valid AddSectorRequest addSectorRequest) {
		Optional<SectorDto> sectorDto = Optional.ofNullable(sectorService
				.addSector(addSectorRequest.getName(), addSectorRequest.getParentId()));
		if (sectorDto.isPresent()) {
			return Response.ok().setPayload(sectorDto.get());
		}
		if (addSectorRequest.getParentId() != null) {
			return Response.badRequest().setErrors(
					String.format("Unable to add sector with name: %s under parent with id: %s",
							addSectorRequest.getName(), addSectorRequest.getParentId()));
		}
		return Response.badRequest()
				.setErrors(String.format("Unable to add sector with name; %s", addSectorRequest.getName()));
	}
}
