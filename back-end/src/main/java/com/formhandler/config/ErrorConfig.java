package com.formhandler.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@AllArgsConstructor
@PropertySource("classpath:error.properties")
@ComponentScan(basePackages = { "com.formhandler" })
public class ErrorConfig {
	private Environment env;

	public String getConfigValue(String configKey) {
		return env.getProperty(configKey);
	}
}
