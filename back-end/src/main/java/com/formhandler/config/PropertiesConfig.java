package com.formhandler.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@PropertySource("classpath:application.properties")
public class PropertiesConfig {
	private Environment env;

	public String getConfigValue(String configKey) {
		return env.getProperty(configKey);
	}
}
