package com.formhandler.exception;

public enum EntityType {
	FORM,
	FORMSECTOR,
	SECTOR
}
