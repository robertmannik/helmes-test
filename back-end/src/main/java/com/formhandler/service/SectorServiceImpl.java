package com.formhandler.service;

import com.formhandler.dto.sector.SectorDto;
import com.formhandler.dto.sector.SectorDtoMapper;
import com.formhandler.exception.EntityType;
import com.formhandler.exception.ExceptionType;
import com.formhandler.exception.FormHandlerException;
import com.formhandler.model.Sector;
import com.formhandler.repository.SectorDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class SectorServiceImpl implements SectorService {
	private SectorDao sectorDao;

	@Override
	@Transactional
	public List<SectorDto> getAllSectors() {
		return sectorDao.getAllSectors().stream().map(SectorDtoMapper::toSectorDto).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public SectorDto addSector(String name, UUID parent_id) {
		Sector sectorModel = new Sector().setName(name);
		if (parent_id != null) {
			sectorModel.setParent(getSectorById(parent_id));
		}
		return SectorDtoMapper.toSectorDto(sectorDao.addSector(sectorModel));
	}

	@Override
	@Transactional
	public Sector getSectorById(UUID id) {
		Optional<Sector> sectorModel = Optional.ofNullable(sectorDao.getSectorById(id));
		if (sectorModel.isEmpty()) {
			throw exception(EntityType.SECTOR, ExceptionType.ENTITY_NOT_FOUND, id.toString());
		}
		return sectorModel.get();
	}

	private RuntimeException exception(EntityType entityType, ExceptionType exceptionType, String... args) {
		return FormHandlerException.throwException(entityType, exceptionType, args);
	}
}
