package com.formhandler.service;

import com.formhandler.dto.form.FormDto;
import com.formhandler.dto.form.FormDtoMapper;
import com.formhandler.exception.EntityType;
import com.formhandler.exception.ExceptionType;
import com.formhandler.exception.FormHandlerException;
import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import com.formhandler.model.Sector;
import com.formhandler.repository.FormDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FormServiceImpl implements FormService {
	private FormDao formDao;
	private SectorService sectorService;
	private FormSectorService formSectorService;

	@Override
	@Transactional
	public FormDto addOrUpdateForm(UUID id, String name, boolean agreedToTerms, List<UUID> sectorIds) {
		List<Sector> sectors = sectorIds.stream().map(this::getSector).collect(Collectors.toList());

		Form formModel = new Form()
				.setName(name)
				.setAgreedToTerms(agreedToTerms);
		if (id != null) {
			formModel.setId(id);
		}
		formDao.addOrUpdateForm(formModel);
		formSectorService.addOrUpdateFormSectors(formModel, sectors, sectorIds);
		return FormDtoMapper.toFormDto(formModel)
				.setSectorIds(sectorIds);
	}

	@Override
	public FormDto getById(UUID id) {
		Optional<Form> formModel = Optional.ofNullable(formDao.getById(id));
		if (formModel.isEmpty()) {
			throw exception(EntityType.FORM, ExceptionType.ENTITY_NOT_FOUND, id.toString());
		}
		FormDto formDto = FormDtoMapper.toFormDto(formModel.get());
		List<Sector> sectors = formSectorService.getFormSectorsByFormId(id).stream()
				.map(FormSector::getSector)
				.collect(Collectors.toList());
		formDto.setSectorIds(sectors.stream()
				.map(Sector::getId)
				.collect(Collectors.toList()));
		return formDto;
	}

	private Sector getSector(UUID uuid) {
		return sectorService.getSectorById(uuid);
	}

	private RuntimeException exception(EntityType entityType, ExceptionType exceptionType, String... args) {
		return FormHandlerException.throwException(entityType, exceptionType, args);
	}
}
