package com.formhandler.service;

import com.formhandler.dto.form.FormDto;

import java.util.List;
import java.util.UUID;

public interface FormService {
	FormDto addOrUpdateForm(UUID id, String name, boolean agreedToTerms, List<UUID> sectorIds);

	FormDto getById(UUID id);
}
