package com.formhandler.service;

import com.formhandler.dto.formsector.FormSectorDto;
import com.formhandler.dto.formsector.FormSectorDtoMapper;
import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import com.formhandler.model.Sector;
import com.formhandler.repository.FormSectorDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FormSectorServiceImpl implements FormSectorService {
	private FormSectorDao formSectorDao;

	@Override
	public FormSectorDto addFormSector(Form formModel, Sector sectorModel) {
		FormSector formSectorModel = new FormSector().setForm(formModel).setSector(sectorModel);
		formSectorDao.addFormSector(formSectorModel);
		return FormSectorDtoMapper.toFormSectorDto(formSectorModel);
	}

	@Override
	public List<FormSector> getFormSectorsByFormId(UUID id) {
		return formSectorDao.getFormSectorsByFormId(id);
	}

	@Override
	public void addOrUpdateFormSectors(Form form, List<Sector> sectors, List<UUID> sectorIds) {
		List<FormSector> formSectors = getFormSectorsByFormId(form.getId());
		if (formSectors.isEmpty()) {
			sectors.forEach(sector -> addFormSector(form, sector));
		} else {
			List<UUID> existingFormSectorSectorIds = formSectors.stream().map(fs -> fs.getSector().getId()).collect(
					Collectors.toList());
			List<UUID> sectorIdsToRemove = existingFormSectorSectorIds.stream().filter(id -> !sectorIds.contains(id)).collect(
					Collectors.toList());
			List<UUID> sectorIdsToAdd = sectorIds.stream().filter(id -> !existingFormSectorSectorIds.contains(id)).collect(
					Collectors.toList());

			formSectors.forEach(fs -> {
				if (sectorIdsToRemove.contains(fs.getSector().getId())) {
					remove(fs);
				}
			});

			List<Sector> sectorsToBeAdded = sectors.stream().filter(sector -> sectorIdsToAdd.contains(sector.getId())).collect(
					Collectors.toList());

			sectorsToBeAdded.forEach(sector -> addFormSector(form, sector));
		}
	}

	@Override
	public void remove(FormSector formSector) {
		formSectorDao.remove(formSector);
	}
}
