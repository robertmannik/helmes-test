package com.formhandler.service;

import com.formhandler.dto.formsector.FormSectorDto;
import com.formhandler.model.Form;
import com.formhandler.model.FormSector;
import com.formhandler.model.Sector;

import java.util.List;
import java.util.UUID;

public interface FormSectorService {
	public FormSectorDto addFormSector(Form formModel, Sector sectorModel);

	List<FormSector> getFormSectorsByFormId(UUID id);

	public void addOrUpdateFormSectors(Form form, List<Sector> sectors, List<UUID> sectorIds);

	public void remove(FormSector formSector);
}
