package com.formhandler.service;

import com.formhandler.dto.sector.SectorDto;
import com.formhandler.model.Sector;

import java.util.List;
import java.util.UUID;

public interface SectorService {
	List<SectorDto> getAllSectors();

	SectorDto addSector(String name, UUID parent_id);
	Sector getSectorById(UUID id);
}
