package com.formhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormHandlerApplication {
	public static void main(String[] args) {
		SpringApplication.run(FormHandlerApplication.class, args);
	}
}
