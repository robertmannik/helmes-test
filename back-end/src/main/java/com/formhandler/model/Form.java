package com.formhandler.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Data
@Embeddable
@Accessors(chain = true)
@Entity(name = "Form")
@Table(name = "form")
public class Form {
	@Id
	@GeneratedValue
	@Column(columnDefinition = "uuid")
	private UUID id;

	@NotNull
	@Size(min = 2, max = 255, message = "Name must be between 2 and 255 characters long!")
	private String name;

	@NotNull
	@Column(name = "agreed_to_terms")
	private boolean agreedToTerms;

	@OneToMany(mappedBy = "form")
	private List<FormSector> formSectors;
}
