package com.formhandler.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Embeddable
@Accessors(chain = true)
@Entity(name = "Sector")
@Table(name = "sector")
public class Sector {
	@Id
	@GeneratedValue
	@Column(columnDefinition = "uuid")
	private UUID id;

	@NotNull
	@Size(min = 2, max = 255, message = "Name must be between 2 and 255 characters long!")
	private String name;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private Sector parent;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<Sector> subSectors = new ArrayList<>();

	@OneToMany(mappedBy = "sector")
	private List<FormSector> formSectors;
}
