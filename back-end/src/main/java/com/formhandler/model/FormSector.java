package com.formhandler.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Embeddable
@Accessors(chain = true)
@Entity(name = "FormSector")
@Table(name = "formsector")
public class FormSector {
	@Id
	@GeneratedValue
	@Column(columnDefinition = "uuid")
	private UUID id;

	@ManyToOne
	@JoinColumn(name = "form_id")
	Form form;

	@ManyToOne
	@JoinColumn(name = "sector_id")
	Sector sector;
}
