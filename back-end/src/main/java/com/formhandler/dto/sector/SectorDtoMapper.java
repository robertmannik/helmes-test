package com.formhandler.dto.sector;

import com.formhandler.model.Sector;

import java.util.List;
import java.util.stream.Collectors;

public class SectorDtoMapper {
	public static SectorDto toSectorDto(Sector sector) {
		SectorDto sectorDto = new SectorDto()
				.setId(sector.getId())
				.setName(sector.getName())
				.setSubSectors(getSubsectors(sector.getSubSectors()));
		if (sector.getParent() != null) {
			sectorDto.setParentId(sector.getParent().getId());
		}
		return sectorDto;
	}

	private static List<SectorDto> getSubsectors(List<Sector> subSectors) {
		return subSectors.stream().map(SectorDtoMapper::toSectorDto).collect(Collectors.toList());
	}
}
