package com.formhandler.dto.formsector;

import com.formhandler.model.FormSector;

public class FormSectorDtoMapper {
	public static FormSectorDto toFormSectorDto(FormSector formSectorModel) {
		return new FormSectorDto()
				.setId(formSectorModel.getId())
				.setFormId(formSectorModel.getForm().getId())
				.setSectorId(formSectorModel.getSector().getId());
	}
}
