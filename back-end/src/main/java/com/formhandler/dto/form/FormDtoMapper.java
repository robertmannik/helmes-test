package com.formhandler.dto.form;

import com.formhandler.model.Form;

public class FormDtoMapper {
	public static FormDto toFormDto(Form formModel) {
		return new FormDto()
				.setId(formModel.getId())
				.setAgreedToTerms(formModel.isAgreedToTerms())
				.setName(formModel.getName());
	}
}
