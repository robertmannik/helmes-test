package com.formhandler.repository;

import com.formhandler.model.FormSector;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

@Repository
@AllArgsConstructor
@NoArgsConstructor
public class FormSectorDao {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public FormSector addFormSector(FormSector formSectorModel) {
		em.persist(formSectorModel);
		return formSectorModel;
	}

	@Transactional
	public List<FormSector> getFormSectorsByFormId(UUID formId) {
		TypedQuery<FormSector> query = em.createQuery("SELECT fs FROM FormSector fs WHERE fs.form.id = :id", FormSector.class);
		query.setParameter("id", formId);
		return query.getResultList();
	}

	public void remove(FormSector formSector) {
		em.remove(formSector);
	}
}
