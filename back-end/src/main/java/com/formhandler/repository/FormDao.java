package com.formhandler.repository;

import com.formhandler.model.Form;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.UUID;

@Repository
@AllArgsConstructor
@NoArgsConstructor
public class FormDao {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public Form addOrUpdateForm(Form formModel) {
		if (formModel.getId() != null) {
			em.merge(formModel);
			return formModel;
		}
		em.persist(formModel);
		return formModel;
	}

	public Form getById(UUID id) {
		TypedQuery<Form> query = em.createQuery(
				"SELECT f FROM Form f WHERE f.id = :id", Form.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}
}
