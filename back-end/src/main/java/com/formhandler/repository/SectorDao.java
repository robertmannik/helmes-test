package com.formhandler.repository;

import com.formhandler.model.Sector;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

@Repository
@AllArgsConstructor
@NoArgsConstructor
public class SectorDao {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Sector> getAllSectors() {
		TypedQuery<Sector> query = em.createQuery("SELECT s FROM Sector s WHERE s.parent IS NULL", Sector.class);
		return query.getResultList();
	}

	@Transactional
	public Sector addSector(Sector sectorModel) {
		em.persist(sectorModel);
		return sectorModel;
	}

	public Sector getSectorById(UUID id) {
		TypedQuery<Sector> query = em.createQuery(
				"SELECT s FROM Sector s WHERE s.id = :id", Sector.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}
}
