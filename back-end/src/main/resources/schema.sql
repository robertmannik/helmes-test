CREATE TABLE sector (
  id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(), -- this presumes CREATE EXTENSION pgcrypto; has been run in the database
  parent_id UUID,
  name VARCHAR(255)
);

CREATE TABLE form (
  id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
  agreed_to_terms BOOLEAN NOT NULL,
  name VARCHAR(255)
);

CREATE TABLE formsector (
  id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
  form_id UUID NOT NULL,
  sector_id UUID NOT NULL,
  FOREIGN KEY (form_id) REFERENCES form ON DELETE CASCADE,
  FOREIGN KEY (sector_id) REFERENCES sector ON DELETE CASCADE
);