--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: form; Type: TABLE; Schema: public; Owner: formhandlerapi
--

CREATE TABLE public.form (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    agreed_to_terms boolean NOT NULL,
    name character varying(255)
);


ALTER TABLE public.form OWNER TO formhandlerapi;

--
-- Name: formsector; Type: TABLE; Schema: public; Owner: formhandlerapi
--

CREATE TABLE public.formsector (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    form_id uuid NOT NULL,
    sector_id uuid NOT NULL
);


ALTER TABLE public.formsector OWNER TO formhandlerapi;

--
-- Name: sector; Type: TABLE; Schema: public; Owner: formhandlerapi
--

CREATE TABLE public.sector (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    parent_id uuid,
    name character varying(255)
);


ALTER TABLE public.sector OWNER TO formhandlerapi;

--
-- Data for Name: form; Type: TABLE DATA; Schema: public; Owner: formhandlerapi
--

COPY public.form (id, agreed_to_terms, name) FROM stdin;
a5223415-68cf-4f29-b085-2805ddf92011	t	test
c438e228-ca58-41f4-8998-fd6dbe9bedda	t	test
21577615-c735-476e-9c2a-9fb51445ac23	t	test
\.


--
-- Data for Name: formsector; Type: TABLE DATA; Schema: public; Owner: formhandlerapi
--

COPY public.formsector (id, form_id, sector_id) FROM stdin;
d6411029-95dd-4b8c-ac04-a5f35305b62f	a5223415-68cf-4f29-b085-2805ddf92011	7609f12e-6b2b-4b98-a137-6b73eb20d5f8
6934029c-22bb-4ad3-ae73-befdab8a2d51	a5223415-68cf-4f29-b085-2805ddf92011	438cdfd6-706e-4150-b836-b5797fcee262
cc634cc4-9174-42a2-b5e6-74870a1279c9	c438e228-ca58-41f4-8998-fd6dbe9bedda	7609f12e-6b2b-4b98-a137-6b73eb20d5f8
34b3e4da-1454-4276-9606-73e5f75b0ef2	c438e228-ca58-41f4-8998-fd6dbe9bedda	438cdfd6-706e-4150-b836-b5797fcee262
4d45b3df-178f-407a-bf8a-26864c5f7823	c438e228-ca58-41f4-8998-fd6dbe9bedda	e2a401ed-15bd-4b44-85bd-3cbf48803e9f
19f0a206-3224-45c5-b386-a729121b9953	c438e228-ca58-41f4-8998-fd6dbe9bedda	70baef6d-729f-4375-9c93-3660d5f2392f
431e8d82-9276-4385-ba7a-9d1c6d1bd9a4	c438e228-ca58-41f4-8998-fd6dbe9bedda	fdb72f97-f21f-4365-8e1a-d125b0ea10e4
6629f151-ad25-4fe2-b0ce-a8e0958b951c	c438e228-ca58-41f4-8998-fd6dbe9bedda	1b45cc53-bf0f-4cde-98d4-83c99a9f7566
7954fd9d-3f4b-4a4b-b227-c0e1d0ae12a0	c438e228-ca58-41f4-8998-fd6dbe9bedda	a98bc110-42a8-490c-940d-4645b998c689
7e0b389f-4d1d-4844-8a73-c14c3a567b78	c438e228-ca58-41f4-8998-fd6dbe9bedda	04cf54a6-ea67-4501-9071-ffdac7a789de
c134835a-b6ae-486a-9479-ec345997cd90	c438e228-ca58-41f4-8998-fd6dbe9bedda	997aaa36-88c7-447d-be0d-67ee7e55b6d8
598da3f3-a67b-4170-8be9-de51883d5b72	c438e228-ca58-41f4-8998-fd6dbe9bedda	124617f9-346c-4595-8a23-5efce2bdb89f
91b1998b-98ec-4236-be22-429f5dbe392a	21577615-c735-476e-9c2a-9fb51445ac23	e2a401ed-15bd-4b44-85bd-3cbf48803e9f
\.


--
-- Data for Name: sector; Type: TABLE DATA; Schema: public; Owner: formhandlerapi
--

COPY public.sector (id, parent_id, name) FROM stdin;
e489819e-bb7d-4dac-b27b-ab6402b7f868	\N	Manufacturing
7609f12e-6b2b-4b98-a137-6b73eb20d5f8	e489819e-bb7d-4dac-b27b-ab6402b7f868	Construction materials
438cdfd6-706e-4150-b836-b5797fcee262	e489819e-bb7d-4dac-b27b-ab6402b7f868	Electronics and Optics
e2a401ed-15bd-4b44-85bd-3cbf48803e9f	e489819e-bb7d-4dac-b27b-ab6402b7f868	Food and Beverage
70baef6d-729f-4375-9c93-3660d5f2392f	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Bakery & confectionery products
fdb72f97-f21f-4365-8e1a-d125b0ea10e4	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Beverages
1b45cc53-bf0f-4cde-98d4-83c99a9f7566	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Fish & fish products
a98bc110-42a8-490c-940d-4645b998c689	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Meat & meat products
04cf54a6-ea67-4501-9071-ffdac7a789de	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Milk & dairy products
997aaa36-88c7-447d-be0d-67ee7e55b6d8	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Other
124617f9-346c-4595-8a23-5efce2bdb89f	e2a401ed-15bd-4b44-85bd-3cbf48803e9f	Sweets & snack food
2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	e489819e-bb7d-4dac-b27b-ab6402b7f868	Furniture
a07780ac-2e94-4bf8-9f2a-a93a0b98675f	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Bathroom/sauna
bf82a787-241a-48a5-ba46-3ac42695bc12	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Bedroom
a0fde687-636b-4cfe-9d29-681effdbd301	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Children's room
b32af348-c5dd-419e-9d18-1c0520c2b963	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Kitchen
687ff9fa-a149-408b-9a93-22850d64a584	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Living room
a8c56c42-0a2b-4edd-afb1-4fc31564467e	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Office
c7e028cc-cb43-4cdd-b2ff-232a4538f03f	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Other (Furniture)
ac72b7cc-147e-4a39-8df8-45a1c1c07bcd	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Outdoor
fe62660e-7a06-43dd-b49b-d909bac2bc3d	2300bb5b-644c-4db4-a0cd-9fcc7ce519c6	Project furniture
261841e6-936f-45f6-b511-bfd3a9c1ec46	e489819e-bb7d-4dac-b27b-ab6402b7f868	Machinery
9e41d30f-8e38-4e0b-830d-a266493b6d66	261841e6-936f-45f6-b511-bfd3a9c1ec46	Machinery components
762c2f99-5e70-4f2d-849e-9fcd8b556e55	261841e6-936f-45f6-b511-bfd3a9c1ec46	Machinery equipment/tools
5eb680f8-30f2-4075-8cc6-711fcf878389	261841e6-936f-45f6-b511-bfd3a9c1ec46	Manufacture of machinery
0d67f00e-603d-4668-a870-34fe276912b0	261841e6-936f-45f6-b511-bfd3a9c1ec46	Maritime
15f5d94e-e971-4ab0-9f3b-a5b86ddcbcbc	0d67f00e-603d-4668-a870-34fe276912b0	Aluminium and steel workboats
1dc8da77-b2f1-4b42-a0b0-8a99b697b4e3	0d67f00e-603d-4668-a870-34fe276912b0	Boat/Yacht building
f7d81630-cc00-4f30-b78b-9e49a4cac393	0d67f00e-603d-4668-a870-34fe276912b0	Ship repair and conversion
36b490d9-11ee-45c1-a0db-d7f2689cc857	261841e6-936f-45f6-b511-bfd3a9c1ec46	Metal structures
77ae9883-4eef-41d3-9503-1ded51a0207a	261841e6-936f-45f6-b511-bfd3a9c1ec46	Repair and maintenance service
c7e78828-1c08-40f0-90e2-2c11fb9caa6c	e489819e-bb7d-4dac-b27b-ab6402b7f868	Metalworking
0a4ec28b-d730-4a52-a755-1825267a5b64	c7e78828-1c08-40f0-90e2-2c11fb9caa6c	Construction of metal structures
aa964c23-6b2d-4d19-b6cc-2f6ae3d6b38d	c7e78828-1c08-40f0-90e2-2c11fb9caa6c	Houses and buildings
3fc76476-e59e-4d86-bf39-e37a58066192	c7e78828-1c08-40f0-90e2-2c11fb9caa6c	Metal products
c8f7ce58-e641-4abd-8486-e0e46a7bd5ef	c7e78828-1c08-40f0-90e2-2c11fb9caa6c	Metal works
cc17b9e2-f961-4a2c-82ed-87b233284118	c8f7ce58-e641-4abd-8486-e0e46a7bd5ef	CNC-machining
909c4067-efcf-432a-be9d-6eb972cc1b81	c8f7ce58-e641-4abd-8486-e0e46a7bd5ef	Forgings, Fasteners
17180df5-a72e-4d9e-b4ba-936b6263d8cf	c8f7ce58-e641-4abd-8486-e0e46a7bd5ef	Gas, Plasma, Laser cutting
3b6ed9a1-6501-4d64-8712-ff4096c1bdb2	c8f7ce58-e641-4abd-8486-e0e46a7bd5ef	MIG, TIG, Aluminum welding
41603223-e2b6-4b42-9d09-760c07c7b1fa	e489819e-bb7d-4dac-b27b-ab6402b7f868	Plastic and Rubber
4fa8039b-6009-4aca-954e-fff949fd6e66	41603223-e2b6-4b42-9d09-760c07c7b1fa	Packaging
f17c348d-e277-449b-b08f-169d5a1e538a	41603223-e2b6-4b42-9d09-760c07c7b1fa	Plastic goods
fd9467b6-c626-4075-b41f-e7cfc9317aad	41603223-e2b6-4b42-9d09-760c07c7b1fa	Plastic processing technology
973d73b6-4e43-4207-ac5a-d8b317ff6201	fd9467b6-c626-4075-b41f-e7cfc9317aad	Blowing
76d8aed7-3c21-4370-a4fd-a474239c42e4	fd9467b6-c626-4075-b41f-e7cfc9317aad	Moulding
e3e18f2a-79e7-412d-93a1-f3ad0f3f920e	fd9467b6-c626-4075-b41f-e7cfc9317aad	Plastics welding and processing
21202bbf-b934-4618-a422-babe6c9969bb	41603223-e2b6-4b42-9d09-760c07c7b1fa	Plastic profiles
b69a5069-30d8-4e17-b315-fcf2aa1b6eaf	e489819e-bb7d-4dac-b27b-ab6402b7f868	Printing
bb13a997-dff6-404b-92af-acf76035c998	b69a5069-30d8-4e17-b315-fcf2aa1b6eaf	Advertising
8bbb330c-1c21-4f4a-b105-b4ab09eae121	b69a5069-30d8-4e17-b315-fcf2aa1b6eaf	Book/Periodicals printing
a7d20ce7-e134-4d07-8370-6ab4be811c39	b69a5069-30d8-4e17-b315-fcf2aa1b6eaf	Labelling and packaging printing
943bf8a2-4cbf-4618-b2d4-c8d3c63b9d45	e489819e-bb7d-4dac-b27b-ab6402b7f868	Textile and Clothing
206c9fc8-8aa6-4b43-8a23-215f5ac557d3	943bf8a2-4cbf-4618-b2d4-c8d3c63b9d45	Clothing
92948cc7-8a42-400f-a29c-923476e95c2e	943bf8a2-4cbf-4618-b2d4-c8d3c63b9d45	Textile
86742e0e-22e1-410f-9f26-0c8f08120313	e489819e-bb7d-4dac-b27b-ab6402b7f868	Wood
cfba3e93-eb45-4bf7-995f-527c5588b850	86742e0e-22e1-410f-9f26-0c8f08120313	Other (Wood)
ac67458b-b9b1-46bd-9806-d748a9154576	86742e0e-22e1-410f-9f26-0c8f08120313	Wooden building materials
2374cc47-80ba-4f79-b79e-bc41898ba3cc	86742e0e-22e1-410f-9f26-0c8f08120313	Wooden houses
2744d0fb-2e65-4255-9acc-3b2142e42bd8	\N	Other
346453c9-7146-4531-be55-c3e4d0293dbf	2744d0fb-2e65-4255-9acc-3b2142e42bd8	Creative industries
f8353168-6d2d-498a-b601-dad930155515	2744d0fb-2e65-4255-9acc-3b2142e42bd8	Energy technology
8506bbe2-c4d0-4c83-b533-791d87f46700	2744d0fb-2e65-4255-9acc-3b2142e42bd8	Environment
2942633d-f9aa-4136-8dee-bf8246472906	\N	Service
e3d06068-688d-41a4-ae61-bf626920e876	2942633d-f9aa-4136-8dee-bf8246472906	Business services
d690d596-8b97-4077-b853-a374d7854f17	2942633d-f9aa-4136-8dee-bf8246472906	Engineering
8a7bd883-9a4a-4560-b6f4-338e19131ba4	2942633d-f9aa-4136-8dee-bf8246472906	Information Technology and Telecommunications
6ceeab17-bad2-446b-98ac-8e662c8547cc	8a7bd883-9a4a-4560-b6f4-338e19131ba4	Data processing, Web portals, E-marketing
36e36dcf-eca7-4b48-bb0b-367ddf69ff92	8a7bd883-9a4a-4560-b6f4-338e19131ba4	Programming, Consultancy
220cef8a-91bb-4f61-a942-6f367c91f51d	8a7bd883-9a4a-4560-b6f4-338e19131ba4	Software, Hardware
9e10f9b3-feca-4386-baed-0e1ec62298fb	8a7bd883-9a4a-4560-b6f4-338e19131ba4	Telecommunications
722972f3-fcd2-471a-822e-3a9060513779	2942633d-f9aa-4136-8dee-bf8246472906	Tourism
7278f26a-b5be-4908-850a-92e8fe35708e	2942633d-f9aa-4136-8dee-bf8246472906	Translation services
34d84383-f416-479d-bdee-0cb9f90afa17	2942633d-f9aa-4136-8dee-bf8246472906	Transport and Logistics
755705bb-7ffc-40a0-b8eb-874760cab7a3	34d84383-f416-479d-bdee-0cb9f90afa17	Air
dd1a6975-c13e-41ce-b299-772d22328c58	34d84383-f416-479d-bdee-0cb9f90afa17	Rail
c43579a6-594b-472d-affd-de07f2f83361	34d84383-f416-479d-bdee-0cb9f90afa17	Road
bc314da0-2559-41be-b36e-1efb248674e2	34d84383-f416-479d-bdee-0cb9f90afa17	Water
\.


--
-- Name: form form_pkey; Type: CONSTRAINT; Schema: public; Owner: formhandlerapi
--

ALTER TABLE ONLY public.form
    ADD CONSTRAINT form_pkey PRIMARY KEY (id);


--
-- Name: formsector formsector_pkey; Type: CONSTRAINT; Schema: public; Owner: formhandlerapi
--

ALTER TABLE ONLY public.formsector
    ADD CONSTRAINT formsector_pkey PRIMARY KEY (id);


--
-- Name: sector sector_pkey; Type: CONSTRAINT; Schema: public; Owner: formhandlerapi
--

ALTER TABLE ONLY public.sector
    ADD CONSTRAINT sector_pkey PRIMARY KEY (id);


--
-- Name: formsector formsector_form_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: formhandlerapi
--

ALTER TABLE ONLY public.formsector
    ADD CONSTRAINT formsector_form_id_fkey FOREIGN KEY (form_id) REFERENCES public.form(id) ON DELETE CASCADE;


--
-- Name: formsector formsector_sector_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: formhandlerapi
--

ALTER TABLE ONLY public.formsector
    ADD CONSTRAINT formsector_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES public.sector(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

