# README #

Front-end made with Angular 9.1, back-end with Java 11 + Gradle + Spring.

### How to run ###

* ```ng serve``` in front-end directory to run angular on localhost:8080.
* ```./gradlew bootRun``` in back-end directory to run spring application on port 8081.
* Spring application presumes there is a PostgreSQL database (formhandler) running on port 5432, username and password can be found in application.properties.
